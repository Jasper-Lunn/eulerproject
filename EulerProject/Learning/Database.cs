﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Learning
{
    class Database
    {
        private static void DatabaseTests()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = "";

                using (SqlCommand cmd = new SqlCommand("Insert into FirstTable values(@Id,@FirstName,@Lastname)", conn))
                {
                    cmd.Parameters.AddWithValue("@Id", 1);
                    cmd.Parameters.AddWithValue("@FirstName", "Jasper");
                    cmd.Parameters.AddWithValue("@Lastname", "Lunn");

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
