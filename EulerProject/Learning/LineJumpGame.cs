﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Learning
{
    /// <summary>
    /// Platformer class. The platformer should be able to jump left and right over one space at a time,
    /// and when it does the tile it was stood on should fall away. If there is no tile two spaces away
    /// he doesn't move from it's current space. It should be able to report it's position.
    /// </summary>
    public class Platformer
    {
        public List<int> Tiles { get; set; }
        public int CharacterIndex { get; set; }
        public int LastIndex
        {
            get
            {
                if (Tiles != null && Tiles.Count >= 1)
                {
                    return Tiles.Count - 1;
                }
                return 0;
            }
        }

        public Platformer(int numberOfTiles, int position)
        {
            Tiles = Enumerable.Range(0, numberOfTiles).ToList();
            CharacterIndex = position >= LastIndex ? LastIndex : position;
        }

        public void JumpLeft()
        {
            var oldIndex = CharacterIndex;
            var newIndex = CharacterIndex - 2;

            if (newIndex >= 0)
            {
                var newValue = Tiles[newIndex];
                Tiles.RemoveAt(oldIndex);
                CharacterIndex = Tiles.FindIndex(x => x == newValue);
            }
        }

        public void JumpRight()
        {
            var oldIndex = CharacterIndex;
            var newIndex = CharacterIndex + 2;

            if (newIndex <= LastIndex)
            {
                var newValue = Tiles[newIndex];
                Tiles.RemoveAt(oldIndex);
                CharacterIndex = Tiles.FindIndex(x => x == newValue);
            }
        }

        public int Position()
        {
            return Tiles[CharacterIndex];
        }

        public static void Example()
        {
            var platformer = new Platformer(6, 3);
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());

            UnitTests();
        }

        //-------------------------------------------
        // Some unit tests
        public static void UnitTests()
        {
            Console.WriteLine("Test1 - " + Test1());
            Console.WriteLine("Test2 - " + Test2());
            Console.WriteLine("Test3 - " + Test3());
        }

        public static bool Test1()
        {
            Platformer platformer = new Platformer(6, 3);
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 1)
            {
                return false;
            }

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 4)
            {
                return false;
            }

            return true;
        }

        public static bool Test2()
        {
            // Max index of tiles is 9
            Platformer platformer = new Platformer(10, 9);
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 7)
            {
                return false;
            }

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());
            // Platformer shouldn't move, as he cannot move two spaces
            if (platformer.Position() != 7)
            {
                return false;
            }

            return true;
        }

        public static bool Test3()
        {
            // Max index of tiles is 9
            Platformer platformer = new Platformer(1000, 500);
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 498)
            {
                return false;
            }

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 501)
            {
                return false;
            }

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 497)
            {
                return false;
            }

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 502)
            {
                return false;
            }

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 496)
            {
                return false;
            }

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());
            if (platformer.Position() != 503)
            {
                return false;
            }

            return true;
        }

        public static void RunPlatformer()
        {
            var platformer = new Platformer(6, 3);
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());

            platformer.JumpLeft();
            Console.WriteLine(platformer.Position());

            platformer.JumpRight();
            Console.WriteLine(platformer.Position());

            Platformer.UnitTests();

            Console.ReadKey();
        }
    }
}
