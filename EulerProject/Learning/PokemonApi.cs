﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Learning
{
    class PokemonApi
    {
        public static void PrintFirePokemon()
        {
            string url = "https://pokeapi.co/api/v2/type/fire/";
            //string urlParameters = "ability/type/fire/";

            HttpClient client = new HttpClient();

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(url).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                var dataObjects = response.Content.ReadAsStringAsync().Result;

                var check = JsonConvert.DeserializeObject<dynamic>(dataObjects);

                JObject d = JObject.Parse(dataObjects);

                var firePokemon = d["pokemon"]?.Children().Select(x => x["pokemon"]["name"].ToString());
                var listPokemon = firePokemon.ToList();
                foreach (var pokemon in listPokemon)
                {
                    Console.WriteLine(pokemon);
                }

                //Console.WriteLine(check.pokemon);
                //Console.WriteLine(d["pokemon"]);
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Wrong");
                Console.ReadKey();
            }
        }



        public static void MakeApiCalls()
        {
            string url = "https://pokeapi.co/api/v2/pokemon/";

            // Add an Accept header for JSON format.
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            var dataObjects = client.GetStringAsync(url).Result;
            JObject data = JObject.Parse(dataObjects);

            var firstLetter = Console.ReadKey().KeyChar;
            Console.WriteLine();
            var relevantPokemon = data["results"].Children()
                .Where(x => Convert.ToChar(x["name"].ToString().FirstOrDefault()) == firstLetter);

            var randomPokemon = relevantPokemon.ElementAt(new Random().Next(1, relevantPokemon.Count()));

            var pokemonData = client.GetStringAsync(randomPokemon["url"].ToString()).Result;
            var pokemonJson = JObject.Parse(pokemonData);

            Console.WriteLine(pokemonJson["name"]);

            // Get type weaknesses

            var typeResponses = pokemonJson["types"].Children().Select(x => 
                                                     client.GetStringAsync(x["type"]["url"].ToString()).Result);
            var types = typeResponses.Select(x => JObject.Parse(x));


            var allWeaknesses = types.SelectMany(x => x["damage_relations"]["double_damage_from"]).Select(y => y["name"]);
            var allResistances = types.SelectMany(x => x["damage_relations"]["half_damage_from"]).Select(y => y["name"]);

            var pokemonWeaknesses = allWeaknesses.Except(allResistances).Distinct().Aggregate((x,y) => x + ", " + y);
            Console.WriteLine("Weaknesses: " + pokemonWeaknesses);

            // Get latest game descriptions (or all?)


            client.Dispose();
        }
    }
}
