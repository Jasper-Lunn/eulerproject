﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler1
    {
        public static void Solution()
        {
            Console.WriteLine(string.Format("The sum of multiples of 3 or 5 below 1000 is {0}",
                Enumerable.Range(0, 1000).Where(x => x % 5 == 0 || x % 3 == 0).Distinct().Sum()));
        }

        public static void FizzBuzz()
        {
            Console.WriteLine(Enumerable.Range(0, 100)
                    .Select(x => x % 15 == 0 ? "FizzBuzz" : x % 5 == 0 ? "Buzz" : x % 3 == 0 ? "Fizz" : x.ToString())
                    .Aggregate((x, y) => x + y + Environment.NewLine));
        }
    }
}
