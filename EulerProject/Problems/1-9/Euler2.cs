﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler2
    {
        public static void Solution()
        {
            var fibonacci = Enumerable.Empty<int>().ToList();
            var count = 1;
            while (count <= 4000000)
            {
                fibonacci.Add(count);
                count += count < 2 ? 1 : fibonacci[fibonacci.Count - 2];
            }

            Console.WriteLine(string.Format("The sum of even values in the Fibonacci sequence below 4 million is {0}", fibonacci.Where(x => x % 2 == 0).Sum()));
        }
    }
}
