﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler3
    {
        public static void Solution()
        {
            var result = long.MinValue;

            // Largest prime factor of 600851475143
            var number = 600851475143;
            var primes = Enumerable.Repeat((long)2, 1).ToList();
            var primeCount = 0;
            for (long i = 3; i < Math.Ceiling(number * 0.5); i += 2)
            {
                // To check number is prime ------------
                if (i > 2 && i % 2 != 0)
                {
                    // get square root of number
                    // get integer above it
                    var integer = Math.Ceiling(Math.Sqrt(i));

                    // if not divisible by each previous prime, it is prime
                    var prime = !primes.Any(x => i % x == 0);
                    if (prime)
                    {
                        primes.Add(i);
                    }
                }

                // If the current number is confirmed to be prime
                if (primes.Count > primeCount)
                {
                    var division = number;
                    for (int j = 0; j < primes.Count;)
                    {
                        // Check the number is divisible by the prime
                        if (division % primes[j] == 0)
                        {
                            // set the variable to the new divided value
                            division = division / primes[j];
                        }
                        else
                        {
                            // if the number is no longer fully divisible then use the next prime
                            j++;
                        }
                    }

                    // If the primes successfully divide the number fully
                    if (division == 1)
                    {
                        result = primes.Last();
                        break;
                    }

                    // if not then find a new prime
                    primeCount = primes.Count;
                }
            }

            Console.WriteLine(string.Format("The largest prime factor of 600851475143 is {0}", result));
        }
    }
}
