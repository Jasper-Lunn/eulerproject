﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    public class Euler4
    {
        public static void Solution()
        {
            // use high 3 digit numbers to start finding multiples and run them through palindrome
            var foundPalindrome = false;
            var threeDigits = 999;
            var product = 0;
            var multiplier = 0;

            while (foundPalindrome == false && threeDigits > 900)
            {
                for (int i = 999; i > 900 && !foundPalindrome; i--)
                {
                    product = threeDigits * i;
                    Console.WriteLine(string.Format("{0} x {1} = {2}", threeDigits, i, product));

                    if (HelperFunctions.IsPalindrome(product))
                    {
                        foundPalindrome = true;
                        multiplier = i;
                    }
                }

                if (!foundPalindrome)
                {
                    threeDigits -= 1;
                }
            }

            Console.WriteLine(string.Format("{0} multiplied by {1} is {2}", threeDigits, multiplier, product));
        }
    }
}
