﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler5
    {
        public static void Solution()
        {
            // Foreach number from 1 to 20, divide a number by that number and confirm that there is no remainder
            int smallestMultiple = 0;
            int count = 1;

            // Started by considering the orignal (simpler) problem
            while (smallestMultiple == 0 && count <= 1000000)
            {
                var fullyDivisible = true;
                Console.WriteLine(count);

                for (int i = 1; i <= 10; i++)
                {
                    if (!HelperFunctions.IsDivisble(count, i))
                    {
                        fullyDivisible = false;
                        break;
                    }
                }

                if (fullyDivisible)
                {
                    smallestMultiple = count;
                    break;
                }

                // End by increasing the count
                count++;
            }

            Console.WriteLine(string.Format("Result is {0}", smallestMultiple));
            Console.WriteLine();

            // My method to find result
            var result = Enumerable.Range(1, 1000000000).FirstOrDefault(x => Enumerable.Range(1, 20).All(i => HelperFunctions.IsDivisble(x, i)));
            Console.WriteLine(string.Format("Result is {0}", result));
        }
    }
}
