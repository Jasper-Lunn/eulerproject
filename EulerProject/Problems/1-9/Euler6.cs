﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler6
    {
        public static void Solution()
        {
            var firstSum = Enumerable.Range(1, 100).Select(x => Math.Pow(x, 2)).Sum();
            var nextSum = Math.Pow(Enumerable.Range(1, 100).Sum(), 2);

            var difference = nextSum - firstSum;
            Console.WriteLine(string.Format("Result is {0}", difference));
        }

        public static void Example()
        {
            var firstSum = Enumerable.Range(1, 10).Select(x => Math.Pow(x, 2)).Sum();
            var nextSum = Math.Pow(Enumerable.Range(1, 10).Sum(), 2);

            var difference = nextSum - firstSum;
            Console.WriteLine(string.Format("Result is {0}", difference));
        }
    }
}
