﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler7
    {
        public static List<long> Primes { get; set; }

        public static void Solution()
        {
            Primes = Enumerable.Empty<long>().ToList();
            var result = Enumerable.Range(1, 10001).Select(x => NextPrime(x)).Last();

            Console.WriteLine(string.Format("Result is {0}", result));
        }

        private static long NextPrime(int nextIndex)
        {
            var latestPrime = Primes.Any() ? Primes.Last() : 1;
            var count = nextIndex <= Primes.Count ? Primes[nextIndex] : latestPrime;

            while (count <= latestPrime + 1000000)
            {
                if (!Primes.Any(x => x == count) && HelperFunctions.IsPrime(count))
                {
                    Primes.Add(count);
                    return count;
                }

                count++;
            }

            return 0;
        }
    }
}
