﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler9
    {
        public static int SumValue = 1000;

        public static void Solution()
        {
            // Limit the checks so that while a + b + c <= 1000

            for (int a = 1; a < 500; a++)
            {
                for (int b = 0; b < 500; b++)
                {

                    if (ValidationFunction(a,b) == 500)
                    {
                        var aSquared = Math.Pow(a, 2);
                        var bSquared = Math.Pow(b, 2);
                        var cSquared = aSquared + bSquared;
                        var c = Math.Sqrt(cSquared);

                        if (a + b + c == SumValue && aSquared + bSquared == cSquared)
                        {
                            var product = a * b * c;
                            Console.WriteLine(string.Format("Result is {0}, for the Pythagorean triplet of {1} + {2} + {3} = {4}", product, a, b, c, SumValue));
                            return;
                        }
                    }

                }
            }
        }

        private static double ValidationFunction(int a, int b)
        {
            return a + b - ((a * b) / SumValue);
        }
    }
}
