﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler10
    {
        public static void Solution()
        {
            // Ensure numbers are totalled as a 'long' otherwise an overflow error will occur for the max value of an 'int'
            long value = Enumerable.Range(1, 2000000).Where(x => HelperFunctions.IsPrime(x)).Sum(x => Convert.ToInt64(x));

            Console.WriteLine(string.Format("Result is {0}", value));
        }
    }
}
