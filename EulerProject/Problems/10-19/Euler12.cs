﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler12
    {
        public static void Solution()
        {
            var result = Enumerable.Range(1, 1000000).FirstOrDefault(x => HelperFunctions.NumberOfFactors(TriangleNumberInSequence(x)) >= 500);
            Console.WriteLine(string.Format("The first triangle number to have over 500 divisors is {0}", TriangleNumberInSequence(result)));
        }

        private static int TriangleNumberInSequence(int n)
        {
            Console.WriteLine(n);
            return n * (n + 1) / 2;
        }
    }
}
