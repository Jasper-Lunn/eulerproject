﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace EulerProject.Problems
{
    class Euler15
    {
        // Effectively it's a 'choose' probability I think

        // order doesn't matter and each time there are only two options

        // so 40 C 20 = 137846528820

        // Realising that the edge of the grid has 40 sections to it
        // This means that to get from the top left to the bottom right,
        // (if we are only using right and down)
        // there are 20 decisions to be made as you cannot go back along a route

        // Solved this using mathematical logic - including a coded solution anyway
        // Got the answer without code on my first try

        public static void Solution()
        {
            // x choose y
            var result = HelperFunctions.Combinations(40, 20);

            Console.WriteLine(string.Format("Result for problem 15 is {0}", result));
        }
    }
}
