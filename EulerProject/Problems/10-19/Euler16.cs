﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler16
    {
        public static void Solution()
        {
            var powerValue = BigInteger.Pow(BigInteger.Parse("2"), 1000).ToString().Select(x => (int)Char.GetNumericValue(x)).Sum();

            Console.WriteLine(string.Format("Result for problem 16 is {0}", powerValue));
        }
    }
}
