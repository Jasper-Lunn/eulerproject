﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler18
    {
        public static void Solution()
        {
            var triangle = new List<List<int>>();

            triangle.Add("75".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("95 64".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("17 47 82".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("18 35 87 10".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("20 04 82 47 65".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("19 01 23 75 03 34".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("88 02 77 73 07 63 67".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("99 65 04 28 06 16 70 92".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("41 41 26 56 83 40 80 70 33".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("41 48 72 33 47 32 37 16 94 29".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("53 71 44 65 25 43 91 52 97 51 14".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("70 11 33 28 77 73 17 78 39 68 17 57".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("91 71 52 38 17 14 91 43 58 50 27 29 48".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("63 66 04 68 89 53 67 30 73 16 69 87 40 31".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("04 62 98 27 23 09 70 98 73 93 38 53 60 04 23".Split(' ').Select(x => Convert.ToInt32(x)).ToList());

            var result = SpecificHelperFunctions.CalculateMax(triangle);
            Console.WriteLine(string.Format("Result for problem 18 is {0}", result));
        }

        public static void Example()
        {
            var triangle = new List<List<int>>();

            triangle.Add("3".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("7 4".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("2 4 6".Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            triangle.Add("8 5 9 3".Split(' ').Select(x => Convert.ToInt32(x)).ToList());

            var result = SpecificHelperFunctions.CalculateMax(triangle);
            Console.WriteLine(string.Format("Result for example 18 is {0}", result));
        }
    }
}
