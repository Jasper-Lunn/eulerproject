﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler19
    {
        public static void Solution()
        {
            var dateCount = new DateTime(1901, 1, 1); // a monday?
            var sundayCount = 0;

            while (dateCount.Year <= 2000)
            {

                if (dateCount.DayOfWeek == DayOfWeek.Sunday)
                {
                    sundayCount += 1;
                }

                dateCount = dateCount.AddMonths(1);
            }

            Console.WriteLine(string.Format("Result for problem 19 is {0}", sundayCount));
        }
    }
}
