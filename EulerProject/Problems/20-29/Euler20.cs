﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler20
    {
        public static void Solution()
        {
            var result = HelperFunctions.Factorial(100).ToString().Select(x => Char.GetNumericValue(x)).Sum();
            Console.WriteLine(string.Format("Result for problem 20 is {0}", result));
        }
    }
}
