﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler21
    {
        public static void LongSolution()
        {
            // Determine if two numbers are amicable to each other
            // Finding all the amicable numbers using for loops may result in doubling up on found values
            // ie, find (220, 284) and (284, 220)

            var amicableNumbers = new List<int>();

            for (int i = 1; i < 10000; i++)
            {
                for (int j = 1; j < 10000; j++)
                {
                    Console.WriteLine(i + ", " + j);

                    if (!amicableNumbers.Contains(i) && !amicableNumbers.Contains(j) && AreAmicableNumbers(i, j))
                    {
                        amicableNumbers.Add(i);
                        amicableNumbers.Add(j);
                    }
                }
            }

            Console.WriteLine(amicableNumbers.Sum());
        }


        public static void Solution()
        {
            // an alternative is to sum the divisors of each number
            // then check to see if that value is within range and amicable

            var amicableNumbers = new List<int>();

            for (int i = 1; i < 10000; i++)
            {
                Console.WriteLine(i);
                var sumDivisors = HelperFunctions.Divisors(i).Sum();
                if (!amicableNumbers.Contains(i) && sumDivisors < 10000 && AreAmicableNumbers(i, sumDivisors))
                {
                    amicableNumbers.Add(i);
                    amicableNumbers.Add(sumDivisors);
                }
            }

            var result = amicableNumbers.Sum();
            Console.WriteLine(string.Format("Result for problem 21 is {0}", result));
        }


        public static bool AreAmicableNumbers(int x, int y)
        {
            if (x != y && HelperFunctions.Divisors(x).Sum() == y && HelperFunctions.Divisors(y).Sum() == x)
            {
                return true;
            }
            return false;
        }
    }
}
