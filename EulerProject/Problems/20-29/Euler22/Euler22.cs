﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler22
    {
        public static Dictionary<char, int> LetterValues = new Dictionary<char, int>
        {
            {'a', 1},
            {'b', 2},
            {'c', 3},
            {'d', 4},
            {'e', 5},
            {'f', 6},
            {'g', 7},
            {'h', 8},
            {'i', 9},
            {'j', 10},
            {'k', 11},
            {'l', 12},
            {'m', 13},
            {'n', 14},
            {'o', 15},
            {'p', 16},
            {'q', 17},
            {'r', 18},
            {'s', 19},
            {'t', 20},
            {'u', 21},
            {'v', 22},
            {'w', 23},
            {'x', 24},
            {'y', 25},
            {'z', 26}
        };

        public static void Solution()
        {
            // get the names from the txt file and order them
            var filePath = Path.Combine(Environment.CurrentDirectory, @"Problems\20-29\Euler22\p022_names.txt");
            var names = File.ReadAllText(filePath).Replace("\"", string.Empty).Split(',').OrderBy(x => x).ToList();

            // For each name, find the alphabetical value of each letter (ie, a = 1, z = 26)
            // then multiply the value of the name by the position in the list
            // and sum all the values

            // incorrect :(
            var result = names.Select(x => GetNameValue(x) * (names.IndexOf(x) + 1)).Sum();
            Console.WriteLine(string.Format("Result for problem 22 is {0}", result));
        }

        public static int GetNameValue(string name)
        {
            return name.ToLower().Select(x => LetterValues[x]).Sum();
        }
    }
}
