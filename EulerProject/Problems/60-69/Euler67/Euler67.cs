﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler67
    {
        public static void Solution()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, @"Problems\60-69\Euler67\p067_triangle.txt");
            var lines = File.ReadAllLines(filePath);

            var triangle = new List<List<int>>();
            for (int i = 0; i < lines.Length; i++)
            {
                triangle.Add(lines[i].Split(' ').Select(x => Convert.ToInt32(x)).ToList());
            }

            var result = SpecificHelperFunctions.CalculateMax(triangle);
            Console.WriteLine(string.Format("Result for problem 67 is {0}", result));
        }
    }
}
