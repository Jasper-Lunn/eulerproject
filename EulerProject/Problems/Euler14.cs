﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler14
    {
        public static List<int> TriedValues { get; set; }

        public static void Example()
        {
            var list = GenerateSequence(13);
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
        }

        public static void Solution()
        {
            // Look into Collatz conjecture, total stopping times

            // sequence is going to be longest when increasing the value, hence we always want to be extending the sequence when the number is odd

            // once the value is a power of 2 it will decay very quickly



        }

        /// <summary>
        /// Both methods thought about in this function (one is commented out)
        /// are too calculation heavy, and will take a long time.
        /// </summary>
        public static void LongSolution()
        {
            var values = Enumerable.Range(1, 1);
            var triedValues = Enumerable.Empty<int>();
            var count = 0;
            while (count < 1000000)
            {
                triedValues = triedValues.Concat(values);

                var multValues = values.Where(x => x % 3 == 0 || x % 3 == 1).Select(x => x * 2);
                multValues = multValues.Concat(values.Where(x => x % 3 == 2).Select(x => x * 2)).Except(triedValues);

                var divValues = values.Where(x => x % 3 == 2).Select(x => ((2 * x) - 1) / 3).Except(triedValues);

                // assuming that the longest value would come after reducing the sequence
                if (divValues.Where(x => x > 999999).Any())
                {
                    break;
                }

                values = multValues.Concat(divValues);

                //var modCheck = result % 6;
                //if (modCheck == 0 || modCheck == 1 || modCheck == 2
                //    || modCheck == 3 || modCheck == 5)
                //{
                //    result = result * 2;
                //}
                //else if (modCheck == 4)
                //{
                //    result = (result - 1) / 3;
                //    result = result * 2;
                //}
                Console.WriteLine(values.Count());
                count++;
            }


            // Maybe check how to create a function to find this

            // Find a large n where the result is less than a million?

            // Don't think this method is going to work in a sensible way
            //TriedValues = Enumerable.Empty<int>().ToList();
            //var check = Enumerable.Range(1, 999999).OrderByDescending(x => x).Max(x => GenerateSequence(x).Count());
        }

        private static List<int> GenerateSequence(int initialValue)
        {
            var list = new List<int>();
            var value = initialValue;
            var count = 1;
            Console.WriteLine(initialValue);

            while (value != 1 && count <= 10000000 && !TriedValues.Contains(value))
            {
                list.Add(value);
                TriedValues.Add(value);

                if (value % 2 == 0)
                {
                    value = value / 2;
                }
                else
                {
                    value = (3 * value) + 1;
                }

                count++;
            }
            list.Add(value);

            return list;
        }
    }
}
