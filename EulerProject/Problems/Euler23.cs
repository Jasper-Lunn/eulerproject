﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler23
    {
        public static void Solution()
        {
            // proper if sum of divisors = number
            // deficient if sum of divisors = number
            // abundant if sum of divisors = number

            // smallest sum of abundant numbers is 24 = 12 + 12
            // all integers greater than 28123 can be written as the sum of two abundant numbers

            // therefore there are numbers below this that cannot be the sum of two abundant numbers
        }

        public static bool IsPerfect(int x)
        {
            return HelperFunctions.Divisors(x).Sum() == x;
        }

        public static bool IsDeficient(int x)
        {
            return HelperFunctions.Divisors(x).Sum() < x;
        }

        public static bool IsAbundant(int x)
        {
            return HelperFunctions.Divisors(x).Sum() > x;
        }
    }
}
