﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    class Euler24
    {
        public static void Solution()
        {
            var millionth = 1000000;

            var currentSequence = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var nthSequence = NthValueAttempt(currentSequence, millionth);

            Console.WriteLine(nthSequence);
        }

        public static void Examples()
        {
            var numbers = new List<int>() { 0, 1, 2, 5, 3, 3, 0 };
            var currentSequence = numbers.Select(a => a.ToString()).Aggregate((x, y) => x + y);
            Console.WriteLine("Original sequence: " + currentSequence.ToString());

            var nextSequence = GetNextGreatestSequence(numbers).Select(a => a.ToString()).Aggregate((x, y) => x + y);
            Console.WriteLine("Next lexicogrphic sequence: " + nextSequence.ToString());

            // var example = Factoradic(14);
            // var nextExample = Factoradic(349);

            numbers = new List<int>() { 1, 2, 3, 4 };
            var nthValue = NthValueAttempt(numbers, 5);
            Console.WriteLine(nthValue);

            numbers = new List<int>() { 1, 2, 3, 4 };
            nthValue = NthValueAttempt(numbers, 14);
            Console.WriteLine(nthValue);
        }

        public static string NthLexicographicValue(List<int> sequence, int nth)
        {
            // This time following the below resource
            // https://math.stackexchange.com/questions/60742/finding-the-n-th-lexicographic-permutation-of-a-string

            // Need to find some value i such that:
            // nth = i * (length - 1)! + remainder
            // where i is within the sequence

            return string.Empty;
        }

        private static int GetPivotIndex(List<int> sequence)
        {
            int lastIndex = sequence.Count - 1;

            // Find the index i where array[i - 1] < array[i]
            // Else use the final index
            int pivotIndex = lastIndex;
            int? previousValue = null;
            for (int i = lastIndex; i >= 0; i--)
            {
                var currentValue = sequence[i];

                if (previousValue != null && currentValue < previousValue)
                {
                    lastIndex = sequence.IndexOf(previousValue ?? i);
                    pivotIndex = sequence.IndexOf(currentValue);
                    break;
                }

                previousValue = currentValue;
            }
            return pivotIndex;
        }

        private static List<int> GetNextGreatestSequence(List<int> sequence)
        {
            // Resource for the example
            // https://www.nayuki.io/page/next-lexicographical-permutation-algorithm

            int lastIndex = sequence.Count - 1;
            int pivotIndex = GetPivotIndex(sequence);

            // If the pivot index is the final index, then it is already the largest permutation
            if (pivotIndex == lastIndex)
            {
                return sequence;
            }

            var pivotValue = sequence[pivotIndex];
            var suffixStartIndex = pivotIndex + 1;

            // Find the smallest value in the suffix (or the last value)
            var swapIndex = suffixStartIndex;
            for (int i = swapIndex + 1; i <= lastIndex; i++)
            {
                if (sequence[i] <= sequence[swapIndex] && sequence[i] > pivotValue)
                {
                    swapIndex = i;
                }
            }

            // Swap the pivot with the smallest value in the suffix
            var swapValue = sequence[swapIndex];

            sequence[pivotIndex] = swapValue;
            sequence[swapIndex] = pivotValue;

            // Order the suffix from lowest to highest
            var prefix = sequence.GetRange(0, suffixStartIndex);
            var suffix = sequence.GetRange(suffixStartIndex, lastIndex - pivotIndex);

            return prefix.Concat(suffix.OrderBy(x => x)).ToList();
        }

        private static string NthValueAttempt(List<int> sequence, int nth)
        {
            // https://medium.com/@aiswaryamathur/find-the-n-th-permutation-of-an-ordered-string-using-factorial-number-system-9c81e34ab0c8
            // Need to find the factoradic of the length of the sequence
            // This will allow the nth value to be directly found
            var factoradic = Factoradic(nth);

            // Add extra 0s to match the length of the sequence if it is shorter
            // This makes it easier to map the values after
            var templateLength = sequence.Count;
            var difference = templateLength - factoradic.Count;
            var zeros = difference > 0 ? Enumerable.Repeat(0, templateLength - factoradic.Count).ToList() : new List<int>();

            var nthTemplate = zeros.Concat(factoradic).ToList();

            // Now loop through the factoradic
            // For each value, remove the corresponding index value and create a new sequence
            var nthSequence = new List<int>();
            var sequenceCopy = sequence;
            for (int i = 0; i < nthTemplate.Count; i++)
            {
                var index = nthTemplate[i];

                nthSequence.Add(sequenceCopy[index]);
                sequenceCopy.RemoveAt(index);
            }

            return nthSequence.Select(a => a.ToString()).Aggregate((x, y) => x + y);
        }

        private static List<int> Factoradic(int value)
        {
            // Store the remainders after each division in a list
            var factorialRepresentation = new List<int>();
            var currentQuotient = value;

            // For loop to begin breaking down the value
            for (int i = 1; i < 1000000; i++)
            {
                // Include the remainder at each stage to the representation
                var rem = currentQuotient % i;
                factorialRepresentation.Add(rem);

                // Take the quotient of the division and use it at the next stage
                // If it is 0 then it cannot be broken down further
                var quo = currentQuotient / i;
                if (quo == 0)
                {
                    break;
                }
                currentQuotient = quo;
            }
            factorialRepresentation.Reverse();
            return factorialRepresentation;
        }
    }
}
