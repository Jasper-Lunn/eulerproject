﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EulerProject.Problems
{
    public static class HelperFunctions
    {
        public static List<int> Divisors(int x)
        {
            var list = new List<int>();
            var midPoint = (int)Math.Ceiling((double)x / 2);

            for (int i = 1; i <= midPoint; i++)
            {
                if (x % i == 0)
                {
                    list.Add(i);
                }
            }

            return list;
        }

        public static BigInteger Combinations(int n, int p)
        {
            return (Factorial(n)) / ((Factorial(p)) * (Factorial(n - p)));
        }

        public static BigInteger Factorial(int x)
        {
            var total = BigInteger.Parse("1");
            for (int i = 2; i <= x; i++)
            {
                total = total * i;
            }
            return total;
        }

        public static List<int> PrimeFactors(int n)
        {
            var list = new List<int>();
            var value = n;
            var count = 2;

            while (!IsPrime(value) && count <= value)
            {
                if (IsPrime(count) && value % count == 0)
                {
                    list.Add(count);
                    value = value / count;
                }
                else
                {
                    count++;
                }
            }
            list.Add(value);

            return list;
        }

        public static int NumberOfFactors(int n)
        {
            return PrimeFactors(n).GroupBy(x => x).Select(x => x.Count()).Aggregate(1, (x, y) => x * (y + 1));
        }

        public static bool IsPalindrome(int value)
        {
            // try and discover if a number is a palindrome
            var testValue = value.ToString();
            var isPalindrome = true;
            var midPoint = Convert.ToInt32(Math.Floor((double)testValue.Length / 2));

            for (int i = 0; i < (midPoint); i++)
            {
                var endIndex = testValue.Length - i - 1;
                if (testValue[i] != testValue[endIndex])
                {
                    isPalindrome = false;
                }
            }

            // Or alternatively (slower)
            //var reversed = new string(testValue.Reverse().ToArray());
            //isPalindrome = testValue == reversed ? true : false;

            return isPalindrome;
        }

        public static bool IsDivisble(int x, int n)
        {
            return (x % n) == 0;
        }

        public static bool IsPrime(int number)
        {
            return IsPrime(Convert.ToInt64(number));
        }
        public static bool IsPrime(long number)
        {
            if (number == 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Ceiling(Math.Sqrt(number));
            for (int i = 3; i <= boundary; i += 2)
            {
                if (number % i == 0) return false;
            }

            return true;
        }


    }

    public static class SpecificHelperFunctions
    {
        // Used with the triangle problems (E67)
        public static int CalculateMax(List<List<int>> triangle)
        {
            var maxRowIndex = triangle.Count - 1;
            var operativeRow = triangle[maxRowIndex];

            // for each int in a row, check what the max is for the row above
            // loop back, starting at the second to last row
            //----------------------------------------------
            // this can break if it isn't in a triangle format
            // work out how to fix that later
            for (int i = (maxRowIndex - 1); i >= 0; i--)
            {
                var currentRow = triangle[i];
                var maxColumnIndex = currentRow.Count;
                var calculatedRow = new List<int>();

                for (int j = 0; j < maxColumnIndex; j++)
                {
                    var calc1 = currentRow[j] + operativeRow[j];
                    var calc2 = currentRow[j] + operativeRow[j + 1];

                    calculatedRow.Add(Math.Max(calc1, calc2));
                }

                operativeRow = calculatedRow;
            }

            return operativeRow.FirstOrDefault();
        }
    }
}
