﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EulerProject.Learning;
using EulerProject.Problems;

namespace EulerProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Euler13.Solution();

            //Euler11.Solution();

            // Haven't finished 14 yet
            //Euler14.Solution();

            // Need to make a log of interesting problems/solutions
            //Euler15.Solution();
            //Euler16.Solution();
            //Euler17.Solution();
            //Euler18.Solution();
            //Euler19.Solution();
            //Euler20.Solution();
            //Euler21.Solution();
            //Euler22.Solution();
            //Euler67.Solution();

            //PokemonApi.MakeApiCalls();

            Euler24.Examples();
            Euler24.Solution();

            Console.ReadKey();
        }
    }
}
